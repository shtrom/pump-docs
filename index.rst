=====
Start
=====

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   userguide
   faq
   clients

.. toctree::
   :maxdepth: 2
   :caption: Sysadmin Documentation

   sysadmins

.. toctree::
   :maxdepth: 2
   :caption: Developer Documentation

   developers


For now, most of the documentation is at `the old wiki <https://github.com/e14n/pump.io/wiki>`_.

Check out the `community information <https://github.com/e14n/pump.io/wiki/Community>`_.

